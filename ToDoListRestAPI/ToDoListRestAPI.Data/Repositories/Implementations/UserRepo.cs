﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using ToDoListRestAPI.Data.Context;
using ToDoListRestAPI.Data.Entities;

namespace ToDoListRestAPI.Data.Repositories
{
    public class UserRepo : IUserRepo
    {
        protected TodoContext context;
        protected DbSet<User> dbSet;

        public UserRepo(TodoContext context)
        {
            this.context = context;
            dbSet = context.Set<User>();
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            return context.User.FirstOrDefault(u => u.Username == username && u.Password == password);
        }
    }
}
