﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using ToDoListRestAPI.Data.Context;
using ToDoListRestAPI.Data.Entities;

namespace ToDoListRestAPI.Data.Repositories
{
    public class TodoListRepo : ITodoListRepo
    {
        protected TodoContext context;
        protected DbSet<TodoList> dbSet;

        public TodoListRepo(TodoContext context)
        {
            this.context = context;
            dbSet = context.Set<TodoList>();
        }

        public void Create(TodoList item)
        {
            dbSet.Add(item);
        }

        public IQueryable<TodoList> GetAll()
        {
            return dbSet;
        }

        public TodoList GetById(int id)
        {
            return dbSet.Find(id);
        }

        public void Update(TodoList item)
        {
            dbSet.Update(item);
        }

        public void Delete(TodoList item)
        {
            dbSet.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
