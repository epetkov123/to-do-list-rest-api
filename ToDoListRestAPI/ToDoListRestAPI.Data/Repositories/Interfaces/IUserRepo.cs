﻿using ToDoListRestAPI.Data.Entities;

namespace ToDoListRestAPI.Data.Repositories
{
    public interface IUserRepo
    {
        User GetByUsernameAndPassword(string username, string password);
    }
}
