﻿using System.Linq;
using ToDoListRestAPI.Data.Entities;

namespace ToDoListRestAPI.Data.Repositories
{
    public interface ITodoListRepo
    {
        void Create(TodoList item);

        IQueryable<TodoList> GetAll();

        TodoList GetById(int id);

        void Update(TodoList item);

        void Delete(TodoList item);

        void Save();
    }
}
