﻿using System;

namespace ToDoListRestAPI.Data.Entities
{
    public class TodoList
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsComplete { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDue { get; set; }
    }
}
