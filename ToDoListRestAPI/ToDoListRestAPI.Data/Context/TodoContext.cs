﻿using Microsoft.EntityFrameworkCore;
using ToDoListRestAPI.Data.Entities;

namespace ToDoListRestAPI.Data.Context
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
               .HasData(new User
               {
                   Id = 1,
                   Password = "123456",
                   Username = "admin"
               });
        }

        public DbSet<User> User { get; set; }

        public DbSet<TodoList> TodoList { get; set; }
    }
}