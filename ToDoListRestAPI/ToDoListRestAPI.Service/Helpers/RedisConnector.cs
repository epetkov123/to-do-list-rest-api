﻿using StackExchange.Redis;
using System;

namespace ToDoListRestAPI.Service.NewFolder
{
    public class RedisConnector
    {
        private static Lazy<ConnectionMultiplexer> lazyConnection;

        static RedisConnector()
        {
            lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                return ConnectionMultiplexer.Connect("localhost");
            });
        }

        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}