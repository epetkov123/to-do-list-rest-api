﻿using ToDoListRestAPI.Data.Entities;
using ToDoListRestAPI.Data.Repositories;

namespace ToDoListRestAPI.Service.Services
{
    public class UserService : IUserService
    {
        private IUserRepo userRepo;

        public UserService(IUserRepo userRepo)
        {
            this.userRepo = userRepo;
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            return userRepo.GetByUsernameAndPassword(username, password);
        }
    }
}
