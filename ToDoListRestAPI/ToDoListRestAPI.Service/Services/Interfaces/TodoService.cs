﻿using System;
using System.Linq;
using ToDoListRestAPI.Data.Entities;
using ToDoListRestAPI.Data.Repositories;

namespace ToDoListRestAPI.Service.Services
{
    public class TodoService : ITodoService
    {
        private ITodoListRepo todoListRepo;

        public TodoService(ITodoListRepo todoListRepo)
        {
            this.todoListRepo = todoListRepo;
        }

        public void Create(TodoList item)
        {
            item.DateCreated = DateTime.Now;
            todoListRepo.Create(item);
        }

        public IQueryable GetAll()
        {
            return todoListRepo.GetAll();
        }

        public TodoList GetById(int id)
        {
            return todoListRepo.GetById(id);
        }

        public void Update(TodoList item)
        {
            item.DateCreated = DateTime.Now;
            todoListRepo.Update(item);
        }

        public void Delete(int id)
        {
            todoListRepo.Delete(GetById(id));
        }

        public void Save()
        {
            todoListRepo.Save();
        }
    }
}