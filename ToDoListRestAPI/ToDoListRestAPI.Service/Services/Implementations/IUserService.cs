﻿using ToDoListRestAPI.Data.Entities;

namespace ToDoListRestAPI.Service.Services
{
    public interface IUserService
    {
        User GetByUsernameAndPassword(string username, string password);
    }
}