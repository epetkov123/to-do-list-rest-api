﻿using System.Linq;
using ToDoListRestAPI.Data.Entities;

namespace ToDoListRestAPI.Service.Services
{
    public interface ITodoService
    {
        void Create(TodoList item);

        IQueryable GetAll();

        TodoList GetById(int id);

        void Update(TodoList item);

        void Delete(int id);

        void Save();
    }
}
