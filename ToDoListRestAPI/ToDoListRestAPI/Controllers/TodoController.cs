﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using ToDoListRestAPI.Data.Entities;
using ToDoListRestAPI.Models;
using ToDoListRestAPI.Service.Services;

namespace ToDoListRestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [Authorize]
    public class TodoController : Controller
    {
        private ITodoService service;

        private IMapper mapper;

        public TodoController(ITodoService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        [HttpPost]
        public IActionResult Create([FromBody]TodoModel todoModel)
        {
            var item = mapper.Map(todoModel, new TodoList());

            service.Create(item);
            service.Save();

            return CreatedAtRoute("GetTodo", new { id = item.Id }, item);
        }

        [HttpGet]
        public List<TodoModel> GetAll()
        {
            return service.GetAll().ProjectTo<TodoModel>().ToList();
        }

        [HttpGet("{id}", Name = "GetTodo")]
        public TodoModel GetById(int id)
        {
            return mapper.Map(service.GetById(id), new TodoModel());
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]TodoModel todoModel)
        {
            var item = service.GetById(id);
            if (item == null)
            {
                return NotFound();
            }

            item.Name = todoModel.Name;
            item.Description = todoModel.Description;
            item.DateDue = todoModel.DateDue;
            item.DateCreated = todoModel.DateCreated;
            item.IsComplete = todoModel.IsComplete;

            service.Update(item);
            service.Save();

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (service.GetById(id) == null)
            {
                return NotFound();
            }

            service.Delete(id);
            service.Save();

            return Ok();
        }
    }
}