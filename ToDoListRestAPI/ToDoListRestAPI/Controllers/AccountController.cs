﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using ToDoListRestAPI.Models;
using ToDoListRestAPI.Service.NewFolder;
using ToDoListRestAPI.Service.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ToDoListRestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private IConfiguration _config;

        private IUserService _service;

        private IMapper _mapper;

        public AccountController(IConfiguration config, IUserService service, IMapper mapper)
        {
            _config = config;
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Logout()
        {
            var cache = RedisConnector.Connection.GetDatabase();
            cache.StringSet();
        }

        public void InsertInBlacklist()
        {

        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult TokenAuthentication([FromBody]LoginModel loginModel)
        {
            if(loginModel == null)
            {
                return BadRequest("Invalid client request");
            }

            var user = Authenticate(loginModel);
            if(user == null)
            {
                return Unauthorized();
            }

            var tokenString = BuildToken(loginModel);
            return Ok(new { token = tokenString });
        }

        private LoginModel Authenticate(LoginModel loginModel)
        {
            var authenticatedUser = _service
                .GetByUsernameAndPassword(loginModel.Username, loginModel.Password);

            return _mapper.Map(authenticatedUser, new LoginModel());
        }

        private string BuildToken(LoginModel loginModel)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var signinCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(30),
              signingCredentials: signinCredentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}